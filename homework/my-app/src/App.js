import React, { useState } from 'react';
import Button from './components/Button/Button';
import ModalText from './components/Modal/ModalText';
import ModalImage from './components/Modal/ModalImage';

import './App.css';
import './App.scss';

function App() {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
  };

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  };

  const closeFirstModal = () => {
    setIsFirstModalOpen(false);
  };

  const closeSecondModal = () => {
    setIsSecondModalOpen(false);
  };

  return (
    <>
      <div className='button-wrapper'>
        <Button onClick={openFirstModal} className='button-1'>
          Open first modal
        </Button>
        <Button onClick={openSecondModal}>Open second modal</Button>
      </div>

      {isFirstModalOpen && (
        <ModalText text="Text for the first modal" onClose={closeFirstModal}>
hi
        </ModalText>
      )}

      {isSecondModalOpen && (
        <ModalImage
          imageUrl="url_to_image"
          alt="Alt text for image"
          onClose={closeSecondModal}
        >
          {'./image.png'}
        </ModalImage>
      )}
    </>
  );
}

export default App;
