import cx from "classnames"
import  './button.scss'

const Button = (props) => {
  const { type = "button", classNames = "", children = "click me", onClick = () => {} } = props

  return (
    <button onClick={onClick} className={cx('button', classNames)} type={type}>
      {children}
    </button>
  )
}

export default Button
