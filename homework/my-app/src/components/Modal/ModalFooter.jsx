import Button from '../Button/Button'
const ModalFooter = ({firstText, secondaryText, firstClick, secondaryClick})=>{
return (
    <div className="modal-footer">
        {firstText && <Button onClick={firstClick}></Button>}
       {secondaryText && <Button onClick={secondaryClick}></Button>}
    </div>
)
}
export default ModalFooter