import cx from 'classnames'
import ModalBody from './ModalBody'
import ModalClose from './ModalClose'
import ModalFooter from './ModalFooter'
import ModalHeader from './ModalHeader'
import ModalWrapper from './ModalWrapper'
import Modal from './Modal';
const ModalText = ({ text, onClose, children }) => (
    <Modal>
      <ModalWrapper>
        <div className="modal-wrap">
        <ModalHeader>
          <ModalClose onClick={onClose} />
        </ModalHeader>
        <ModalBody>
          <p>{text}</p>
          {children}
        </ModalBody>
        </div>
      </ModalWrapper>
    </Modal>
  );
  export default ModalText;