const ModalWrapper = ({ children }) => (
    <div className="modal-wrapper">
      {children}
    </div>
  );
  export default ModalWrapper;